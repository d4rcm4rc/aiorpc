"""
The MIT License (MIT)

Copyright (c) 2016 Marc André Wittorf (marc-a@gmx.de)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from types import MethodType
from itertools import starmap
import inspect
import asyncio
from .RpcException import *

class BlockingProxy:
	def __init__(self, parent):
		self._parent = parent
	
	def waitConnected(self):
		self._parent._loop.run_until_complete(self._parent.waitConnected())

class RpcClient:
	def __init__(self, endpoint, loop = None):
		self._endpoint = endpoint
		self._loop = loop or asyncio.get_event_loop()
		self._description = []
		self.blocking = BlockingProxy(self)
		self._descriptionreader = self._loop.create_task(self._readDescription())

	def waitConnected(self):
		return self._descriptionreader
	
	async def _readDescription(self):
		self._description = await self._endpoint("DESCRIBE")
		#print(self.description)
		self._addRpcMethods()
		
	def _addRpcMethod(self,m):
		nm = m[0]
		async def rpCall(self, *args, **kwargs):
			try:
				res = await self._endpoint({'call':nm, 'args':args, 'kwargs':kwargs})
			except Exception as e:
				raise RpcCommunicationError() from e
			return self._interpretResponse(res)
		rpCall.__signature__ = self._constructSignature(m)
		rpCall.__name__ = m[0]
		rpCall.__qualname__ = m[0]
		setattr(self,m[0],MethodType(rpCall,self))
		loop = self._loop
		def brpCall(self, *args, **kwargs):
			try:
				res = self._parent._loop.run_until_complete(self._parent._endpoint({'call':nm, 'args':args, 'kwargs':kwargs}))
			except Exception as e:
				raise RpcCommunicationError() from e
			return self._parent._interpretResponse(res)
		brpCall.__signature__ = self._constructSignature(m)
		brpCall.__name__ = m[0]
		brpCall.__qualname__ = m[0]
		setattr(self.blocking,m[0],MethodType(brpCall,self.blocking))
	
	def _addRpcMethods(self):
		for m in self._description:
			self._addRpcMethod(m)
	
	def _interpretResponse(self,response):
		try:
			if response[0]:
				return response[0][0]
		except Exception as e:
			raise RpcFormatError() from e
		try:
			ex = RpcRemoteException(response[1])
		except Exception as e:
			raise RpcFormatError() from e
		raise ex
		
	
	@staticmethod
	def _constructSignature(sigt):
		(name,parameterst,return_annotation) = sigt
		def transformParameters(name,default,kind):
			kind = {0:inspect.Parameter.POSITIONAL_ONLY,
			        1:inspect.Parameter.POSITIONAL_OR_KEYWORD,
			        2:inspect.Parameter.VAR_POSITIONAL,
			        3:inspect.Parameter.KEYWORD_ONLY,
			        4:inspect.Parameter.VAR_KEYWORD,}[kind]
			default = inspect.Parameter.empty if default==None else default[0]
			return inspect.Parameter(name,kind,default=default)
		parameters = list(starmap(transformParameters,parameterst))
		#print(parameters)
		return_annotation = inspect.Signature.empty if return_annotation==None else return_annotation[0]
		return inspect.Signature(parameters,return_annotation=return_annotation)
		
#TODO: add RpcClient that works with blocking endpoints?