"""
The MIT License (MIT)

Copyright (c) 2016 Marc André Wittorf (marc-a@gmx.de)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import asyncio
import inspect
from concurrent.futures import ThreadPoolExecutor
from itertools import starmap
from functools import wraps, partial
from types import MethodType
from .RpcException import *

class RpcServer:
	def __init__(self, servedObject):
		self.served_object = servedObject
		self.members = []

	async def handleCall(self, message, conn):
		if message == "DESCRIBE":
			return self._describe()
		elif isinstance(message, dict):
			try:
				if set(['call','args','kwargs'])-message.keys():
					raise RpcFormatError()
				if message.keys()-set(['call','args','kwargs']):
					raise RpcFormatError()
				if message['call'] not in self.members:
					raise RpcFormatError()
				fn = getattr(self.served_object,message['call'])
				try:
					sig = inspect.signature(fn)
				except ValueError:
					sig=None
				#todo: exception format
				if sig != None and any(x=='_connection' for x in sig.parameters):
					res = fn(_connection=conn,*message['args'], **message['kwargs'])
				else:
					res = fn(*message['args'], **message['kwargs'])
				if asyncio.coroutines.iscoroutine(res):
					res = await res
				return ((res,),)
			except Exception as e:
				return (None, str(type(e))+': "'+str(e)+'"')
	
	def _describe(self):
		members = inspect.getmembers(self.served_object, predicate=inspect.ismethod)
		members = [(x,y) for x,y in members if x[0]!='_' and (inspect.ismethod(y) or inspect.isfunction(y))]
		
		def transformParameter(pt):
			(name,p) = pt
			kind = {p.POSITIONAL_ONLY:0,
			        p.POSITIONAL_OR_KEYWORD:1,
			        p.VAR_POSITIONAL:2,
			        p.KEYWORD_ONLY:3,
			        p.VAR_KEYWORD:4,}[p.kind]
			default = None if p.default==p.empty else (str(p.default),)
			return (name,default,kind)
		
		def lookupArgs(name, fn):
			try:
				sig = inspect.signature(fn)
				ret = None if sig.return_annotation==sig.empty else (str(sig.return_annotation),)
				return (name, list(map(transformParameter,sig.parameters.items())), ret)
			except ValueError:
				return (name,[],None)
		members = list(filter(lambda x:x[0]!='_connection',starmap(lookupArgs, members)))
		self.members = [x[0] for x in members]
		#print(members)
		return members

class LegacyWrapper:
	def _addMethod(self,name,fn):
		@asyncio.coroutine
		@wraps(fn)
		def wrappedMethod(self, *args, **kwargs):
			return self._loop.run_in_executor(self._executor, partial(fn, *args, **kwargs))
		setattr(self,name,MethodType(wrappedMethod,self))
		
	def __init__(self, wrappedObject, maxWorkers=1, *, loop=None):
		self._wrapped_object = wrappedObject
		self._executor = ThreadPoolExecutor(maxWorkers)
		self._loop = loop or asyncio.get_event_loop()
		members = inspect.getmembers(self._wrapped_object, predicate=inspect.ismethod)
		members = [(x,y) for x,y in members if x[0]!='_' and (inspect.ismethod(y) or inspect.isfunction(y))]
		for (name,fn) in members:
			self._addMethod(name,fn)
		
		
		
		