# marc-aiorpc
Simple Message and RPC library for Python >=3.4 unsing asyncio features.

I did not test most code thoroughly enough, there are probably some bugs or even security flaws still in the code. I recommend against using this over the internet.

## Interface
The library consists mainly of the following classes:
 * `aiorpc.MsgServer`: A server for a message service
 * `aiorpc.MsgClient`: The respective client
 * `aiorpc.RpcServer`: A server that takes an object and exposes it over a `MsgServer`
 * `aiorpc.RpcClient`: A client to consume objects served by an `RpcServer`
 * `aiorpc.LegacyWrapper`: A helper class to serve objects that use (legacy) blocking calls

## Example
#### Server
```python
#!/bin/env python3

from aiorpc import MsgServer,RpcServer

class ServedClass:
	def callMe(self):
		return 'hello!'

msgs = MsgServer('tcp:127.0.0.1:1337')
msgs.start()

rpcs = RpcServer(ServedClass())
msgs.registerEndpoint(1,rpcs.handleCall)

msgs.loop.run_forever()
```

#### Client
```python
#!/bin/env python3

from aiorpc import MsgClient,RpcClient

msgc = MsgClient('tcp:127.0.0.1:1337')
msgc.connectBlocking()

rpc = RpcClient(msgc.answerEndpoint(1),msgc.loop)
rpc.blocking.waitConnected()

print(rpc.blocking.callMe())

msgc.loop.run_until_complete(msgc.close())
```

## Concepts
#### Message Format
All messages are JSON-encoded Python dicts containing control information and a payload. The payload is user-defined and directly taken from the `message`-arguments in the `send___Message` methods. Messages can be anything that is JSON-encodable: Dicts, Lists, Tuples and primitive types. For security reasons no arbitrary objects can be passed and will cause errors.  
Messages are currently limited to 64kiB of encoded data, including control information.


#### Message Types
Currently there are three message types:

###### SimpleMessage
The message is sent to the correct endpoint and processed by it.

###### AnsweredMessage
The message is sent to the correct endpoint. The endpoint processes it and answers with a *AnswerMessage*. *AnsweredMessages* contain a unique token that is given to the *AnswerMessage* so every answer can be matched to the correct request.

###### AnswerMessage
This message is sent only as an answer to an *AnsweredMessage*.


#### Endpoint names
A valid endpoint name must have two properties: 1. It must be JSON-encodable and 2. It must be hashable.


#### Interaction between components
Todo... See example for now.


## Reference

In the following, `Future` means an `asyncio.Future`, `Queue` means an `asyncio.Queue`.

Some methods (usually constructors) can be passed a `loop` as argument. This is an `asyncio.BaseEventLoop` that will be used by the method or object. If the argument is not set, the calling thread's default loop is used. If you want to use the method or object with a different loop (eg. on a different thread), pass it in the `loop` argument.

#### Class `MsgClient`
###### `__init__(addr, loop=<default loop>)`
Pass an address to connect to, either `tcp:<host_or_ip>:<port>` for an AF_INET/SOCK_STREAM socket or `unix:/path/to/socket` for an AF_UNIX/SOCK_STREAM socket.

###### *@coroutine* `connect()`
Connect to the server and do the protocol negotiation.

###### `connectBlocking()`
Convenience function that calls `connect()` and run the loop until the connection is established.

###### `close()`
Disconnect from the server. Returns a `Future` that will be set as soon as the connection is closed.

###### `registerEndpoint(k, ep)`
Register an endpoint at the client. `k` is the endpoint identifier and `ep` is a `Future`, a `Queue` or a callback function as described later in *Endpoint Types*.

###### `sendSimpleMessage(endpoint, message)`
Send a *SimpleMessage* to the specified endpoint.

###### `sendAnsweredMessage(endpoint, message)`
Send an *AnsweredMessage* to the specified endpoint. Returns a `Future`. This `Future` will be set to the received answer.

###### `sendAnswerMessage(token, message)`
Send an *AnswerMessage* to answer an *AnsweredMessage*. Useful for `Future`s or `Queue`s as Endpoints. Do not use with Callback-Endpoints. Pass the *answer token* from the message.

###### `answerEndpoint(ep)`
Convenience function that binds an endpoint identifier to the `sendAnsweredMessage` method.


#### Class `SrvMsgClient`
Objects of this class represent connections at the server side. Hence they lack the methods for connecting and should not be constructed directly. All other methods are the same as `MsgClient`.


#### Class `MsgServer`
###### `__init__(addr, loop=<default loop>)`
Pass an address to bind to, either 'tcp:...' or 'unix:...'.

###### *@coroutine* `start()`
Start the server (automatically `async`'s it, no need to schedule manually).

###### `stop()`
Stop the server. Returns a `Future` that can be waited on and will be set once the server has stopped.

###### `registerEndpoint(k, ep)`
Register an endpoint with the server (ie. all connected clients). Arguments are the same as for `MsgClient.registerEndpoint()`.

###### `onConnect(callback)` and `onDisconnect(callback)`
Register callbacks that will be called when a client dis-/connects. The serverside client-object is passed as the only argument.

###### `broadcast(endpoint, message)`
Send a *SimpleMessage* to all connected clients. `endpoint` is an endpoint identifier, `message` the message.

###### `clients`
List of all connected clients as `SrvMsgClient` objects.


#### Class `RpcServer`
###### `__init__(servedObject)`
Pass the object that will be served. All Methods that don't start with an underscore will be exposed. All exposed methods must return quickly or be coroutines. If you want to expose an object with blocking methods, consider wrapping it in a `LegacyWrapper`.

###### `handleCall(message, conn)`
Deconstruct a message and call the correct method. Returns the method's result as message. Usually, you won't want to call this directly and rather register this method as an Endpoint to the `MsgServer`.


#### Class `LegacyWrapper`
###### `__init__(servedObject, maxWorkers=1)`
Pass the object that will be wrapped. Also you can pass the maximum number of worker threads that will be used to call methods from this object. Before you raise this number above 1, make sure that all exposed methods are thread safe.


#### Class `RpcClient`
###### `__init__(endpoint, loop=<default loop>)`
Consume an endpoint (usually an `MsgClient.answerEndpoint`) and offer the methods exposed by an `RpcServer` on this endpoint.

###### `waitConnected()`
Returns a `Future` that will be set when the `RpcClient`-object has fully connected to the server and received all method definitions. Note that no remote method can be called before the connection has been established, so you will want to use this.

###### <Methods exposed by the `RpcServer`>
All methods that are exposed by an `RpcServer` are available in the `RpcClient` once it has connected and received method definitions. The signatures are passed to the client for limited reflection abilities. These are `@coroutine`s 

###### `blocking`
`waitConnected()` and exposed server methods can be called blocking by using `RpcClient.blocking.waitConnected()` or resp. `RpcClient.blocking.<server_method>()`.


#### Endpoint Types
An endpoint is a `Future`, a `Queue` or a callback function. While `Future` and `Queue` will receive the whole Message including headers, as they might be needed for answering, callbacks are only passed the message payload.  
Todo: document concrete message format. For now: use trial and error or look in `_MultipleEndpointMapHandlerMixin` in MsgClient.py.

###### `Future` Endpoint
A `Future` will be set once the endpoint receives a message. It is then removed from the endpoint association, as `Future`s can only be set once. `Future`s as server-wide endpoints should thus not be useful. They can however, sometimes be useful for connection-wide temporary endpoints.

###### `Queue` Endpoint
A `Queue` will be pushed a received message.

###### Callback endpoint
An endpoint callback is a function that accepts at least a message as first positional argument. Optionally it can have `conn` and `endpoint` keyword arguments, in which the connection (a `MsgClient` or `SrvMsgClient` object) and the endpoint name will be passed.  
If the client wants an answer (using `sendAnsweredMessage`), the return value of the callback is sent back as answer. The callback may return a `Future`, in which case the answer is returned once the `Future` is set.  
The callback can be an `asyncio.coroutine`. It is scheduled then automatically.


## Todo
* Implement Queue handler and add sanity checks for Queue and Future
* Add some convenience methods for easy server and client setups
* Timeouts
* SSL
* More extensive example
* Better documentation

## Further Ideas
* Allow simple creation of remote objects and local references to them
* Allow serialization of objects, implement restrictions for security