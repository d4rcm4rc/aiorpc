#!/bin/env python3

from marc_aiorpc import MsgClient,RpcClient,RpcServer

#connect to the server (part 1)
msgc = MsgClient('tcp:127.0.0.1:1337')

#bind an endpoint that the server broadcasts to
msgc.registerEndpoint(255,lambda x: print("received broadcast: %s" % (x,)))

#expose an object for the server
class TestClass:
	def test(self):
		return 'test'
rpcs = RpcServer(TestClass())
msgc.registerEndpoint('reverse',rpcs.handleCall)

#connect to the server (part 2)
msgc.connectBlocking()

#get first remote object
rpc1 = RpcClient(msgc.answerEndpoint(1),msgc.loop)
msgc.loop.run_until_complete(rpc1.waitConnected())

#get second remote object
rpc2 = RpcClient(msgc.answerEndpoint(2),msgc.loop)
rpc2.blocking.waitConnected() #convenience function to avoid the 'run_until_complete' everytime

#call remote functions
print(rpc1.blocking.someBlockingFunction())
print(rpc2.blocking.waitSomeTime())
print(msgc.loop.run_until_complete(rpc2.callMe()))

#consume a raw endpoint
print(msgc.loop.run_until_complete(msgc.sendAnsweredMessage('echo','echotest')))

print(msgc.loop.run_until_complete(msgc.sendAnsweredMessage(3,1)))
print(msgc.loop.run_until_complete(msgc.sendAnsweredMessage(3,2)))
print(msgc.loop.run_until_complete(msgc.sendAnsweredMessage(4,3)))
print(msgc.loop.run_until_complete(msgc.sendAnsweredMessage(4,-4)))
print(msgc.loop.run_until_complete(msgc.sendAnsweredMessage(3,5)))
print(msgc.loop.run_until_complete(msgc.sendAnsweredMessage('test',12)))

msgc.loop.run_until_complete(msgc.close())


