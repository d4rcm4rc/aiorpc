#!/bin/env python3

import asyncio
import time
from marc_aiorpc import MsgServer,RpcServer,RpcClient,LegacyWrapper


#some classes
class ServedLegacyClass:
	def someBlockingFunction(self):
		time.sleep(2)
		return 'slept for 2 seconds!'

class ServedClass:
	def callMe(self):
		return 'hello!'
	
	@asyncio.coroutine
	def waitSomeTime(self):
		yield from asyncio.sleep(2)
		return 'slept asynchronously for 2 seconds!'


#actual example starts here
#==========================

#Start message server
msgs = MsgServer('tcp:127.0.0.1:1337')
msgs.start()


#Serve object
#This object has blocking methods, so we wrap it. We allow eight parallel calls
rpcs1 = RpcServer(LegacyWrapper(ServedLegacyClass(), maxWorkers=1))
msgs.registerEndpoint(1,rpcs1.handleCall)


#Serve another object through the same message connection on another endpoint
rpcs2 = RpcServer(ServedClass())
msgs.registerEndpoint(2,rpcs2.handleCall)


#Bind a method to an endpoint. Note that we can not only use numbers as endpoint identifiers
def echoEndpoint(message):
	return message
msgs.registerEndpoint('echo',echoEndpoint)


#we can use the same method for many endpoints, the name is passed to the callback, if wanted
counter = {3:0,4:0,'test':0}
def extraEndpoint(message, conn, endpoint):
	try:
		counter[endpoint] += message
	except:
		pass
	return counter[endpoint]
msgs.registerEndpoint(3,extraEndpoint)
msgs.registerEndpoint(4,extraEndpoint)
msgs.registerEndpoint('test',extraEndpoint)


#We can be notified on dis-/connecz
msgs.onConnect(lambda conn: print('A client has connected'))
msgs.onDisconnect(lambda conn: print('A client has disconnected'))


#the server can initiate communication
def broadcaster():
	bcaster_handle = msgs.loop.call_later(2,broadcaster)
	msgs.broadcast(255,'Friendly reminder: Please don\'t spam')
bcaster_handle = msgs.loop.call_later(2,broadcaster)


#the server can consume a client supplied object:
@asyncio.coroutine
def demonstrateServerRpc(conn):
	rpc = RpcClient(conn.answerEndpoint('reverse'))
	yield from rpc.waitConnected()
	res = yield from rpc.test()
	print("Client test(): %s" % (res,))
msgs.onConnect(demonstrateServerRpc)


try:
	msgs.loop.run_forever()
except:
	pass

bcaster_handle.cancel()
msgs.loop.run_until_complete(msgs.stop())
