"""
The MIT License (MIT)

Copyright (c) 2016 Marc André Wittorf (marc-a@gmx.de)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import os
import asyncio
from .MsgClient import SrvMsgClient
from .addrparse import parseAddr

class MsgServer:
	
	def __init__(self, addr, loop = None):
		self.loop                 = loop or asyncio.get_event_loop()
		self.addr                 = addr
		self.connect_callbacks    = []
		self.disconnect_callbacks = []
		self.clients              = []
		self.server               = None
		self.global_endpoints     = {}
	
	def start(self):
		async def _start(self):
			addr = parseAddr(self.addr)
			if addr[0]=='unix':
				try:
					os.remove(addr[1])
				except OSError:
					pass
				self.server = await asyncio.start_unix_server(self.handle, addr[1], loop=self.loop)
			elif addr[0]=='tcp':
				self.server = await asyncio.start_server(self.handle, addr[1], addr[2], loop=self.loop)
		return self.loop.create_task(_start(self))
	
	def stop(self):
		self.server.close()
		return self.server.wait_closed()

	def registerEndpoint(self, k, ep):
		self.global_endpoints[k] = ep
	
	def onConnect(self, callback):
		self.connect_callbacks.append(callback)
	
	def onDisconnect(self, callback):
		self.disconnect_callbacks.append(callback)
	
	def broadcast(self, endpoint, message):
		for c in self.clients:
			c.sendSimpleMessage(endpoint, message)

	async def handle(self, reader, writer):
		data = reader.readexactly(4)
		try:
			data = await asyncio.wait_for(data,2)
		except asyncio.TimeoutError:
			writer.close()
		
		protocol = int.from_bytes(data,byteorder='little')
		if protocol == 1:
			conn = SrvMsgClient(reader,writer,global_endpoints=self.global_endpoints,loop=self.loop)
			#conn.registerLocalEndpoint(1,lambda x: x)
			self.clients.append(conn)
			for fn in self.connect_callbacks:
				res = fn(conn)
				if asyncio.coroutines.iscoroutine(res):
					asyncio.ensure_future(res,loop=self.loop)
			await conn.startHandleInput()
			for fn in self.disconnect_callbacks:
				fn(conn)
				if asyncio.coroutines.iscoroutine(res):
					asyncio.ensure_future(res,loop=self.loop)
			self.clients.remove(conn)
			try:
				await writer.drain()
			except:
				pass
			writer.close()
		else:
			writer.close()
