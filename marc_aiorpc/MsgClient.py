"""
The MIT License (MIT)

Copyright (c) 2016 Marc André Wittorf (marc-a@gmx.de)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import asyncio
from json import dumps, loads
from inspect import signature
from functools import partial
from .MsgException import *
from .addrparse import parseAddr

class BaseMsgClient:
	
	def __init__(self, loop = None, *args, **kwargs):
		super(BaseMsgClient,self).__init__(*args, **kwargs)
		self.loop = loop or asyncio.get_event_loop()
	
	def close(self):
		self.writer.close()
		return self.input_handler
	
	def answerEndpoint(self, ep):
		return partial(self.sendAnsweredMessage,ep)
	
	async def handleInput(self):
		try:
			while 1:
				try:
					data = await self.reader.readexactly(2)
				except asyncio.IncompleteReadError as e:
					raise MsgConnClosed() from e
				msglen = int.from_bytes(data,byteorder='little')
				try:
					data = await self.reader.readexactly(msglen)
				except asyncio.IncompleteReadError as e:
					raise MsgConnClosed() from e
				try:
					message = loads(data.decode('utf-8'))
				except Exception as e:
					raise MsgMalformedMessage() from e
				if type(message) != dict:
					raise MsgMalformedMessage()
				if 'a' in message:
					if set(['a','p'])-message.keys():
						raise MsgMalformedMessage()
					if message.keys()-set(['a','p']):
						raise MsgMalformedMessage()
					payload = message['p']
					token   = message['a']
					fut = self.answer_futures[token]
					del self.answer_futures[token]
					fut.set_result(payload)
				elif 'ERROR' in message:
					token = message['ERROR']
					fut = self.answer_futures[token]
					del self.answer_futures[token]
					e = None
					try:
						exceptions = [MsgHandleError, MsgTransmissionError, MsgMalformedMessage]
						exceptions = {x.__name__:x for x in exceptions}
						e = exceptions[message['p'][0]](str(message['p'][1]))
					except:
						e = MsgRemoteException()
					fut.set_exception(e)
				else:
					if set(['e','p'])-message.keys():
						raise MsgMalformedMessage()
					if message.keys()-set(['e','t','p']):
						raise MsgMalformedMessage()
					#if 't' in message:
					self._messageReceived(message)
		except (MsgConnClosed, MsgMalformedMessage) as e:
			for (k,fut) in self.answer_futures.items():
				fut.set_exception(e)
	
	def sendSimpleMessage(self, endpoint, message):
		message = {'e':endpoint, 'p':message}
		self.sendRawMessage(message)
	
	async def sendAnsweredMessage(self, endpoint, message):
		tx = {'e': endpoint, 't': self.answer_token, 'p': message}
		fut = asyncio.Future()
		self.answer_futures[self.answer_token] = fut
		self.answer_token = self.answer_token+1
		self.sendRawMessage(tx)
		return await fut
	
	def sendAnswerMessage(self, token, message):
		message = {'a':token, 'p':message}
		try:
			self.sendRawMessage(message)
		except Exception as e:
			self.sendAnswerException((token,),MsgTransmissionError(e))
	
	def sendAnswerException(self, token, e=None):
		if token:
			message = {'ERROR': token[0], 'p':(type(e).__name__,str(e))}
			self.sendRawMessage(message)
			#raise Exception()

	def sendRawMessage(self, message):
		#TODO: handle error: not serializable
		try:
			msg = bytes(dumps(message),'utf-8')
		except Exception as e:
			raise MsgCannotSerializeError() from e
		l = len(msg).to_bytes(2,byteorder='little')
		self.writer.write(l)
		self.writer.write(msg)

class _LinkEndpointsMixin:
	def __init__(self):
		self.link_endpoints = {}
	
	def registerEndpoint(self, key, ep):
		self.link_endpoints[key] = ep

class _MultipleEndpointMapHandlerMixin:
	def _messageReceived(self, message):
		try:
			token = (message['t'],)
		except:
			token = None
		try:
			for epm in self._getEndpointMaps():
				if message['e'] in epm:
					ep = epm[message['e']]
				else:
					continue
				
				if isinstance(ep,asyncio.Future):
					ep.set_result('res') #todo
					del epm[message['e']]
				elif isinstance(ep,asyncio.Queue):
					pass
				elif callable(ep):
					callback_args=[message['p']]
					callback_kwargs={'conn':self, 'endpoint': message['e']}
					epsig = signature(ep)
					if not any(p.kind == p.VAR_KEYWORD for (n,p) in epsig.parameters.items()):
						callback_kwargs = {k: v for (k,v) in callback_kwargs.items() if (k in epsig.parameters)}
					
					try:
						res = ep(*callback_args,**callback_kwargs)
					except Exception as e:
						raise MsgHandleError() from e
					
					if asyncio.coroutines.iscoroutine(res):
						res = asyncio.ensure_future(res,loop=self.loop)
					
					if token != None:
						if isinstance(res,asyncio.Future):
							def cb(fut):
								try:
									self.sendAnswerMessage(token[0],fut.result())
								except Exception as e:
									try:
										self.sendAnswerException(token, MsgHandleError(e))
									except:
										self.input_handler.cancel()
							res.add_done_callback(cb)
						else:
							self.sendAnswerMessage(token[0],res)
				return
			raise MsgNoEndpointException()
		except MsgException as e:
			self.sendAnswerException(token, e)


class MsgClient(BaseMsgClient, _LinkEndpointsMixin, _MultipleEndpointMapHandlerMixin):
	PROTOCOL_TYPE = 1
	
	def __init__(self, addr, *args, **kwargs):
		super(MsgClient, self).__init__(*args, **kwargs)
		self.addr   = addr
		self.reader = None
		self.writer = None
		self.answer_token = 0
		self.answer_futures = {}
		self.input_handler = None
	
	async def connect(self):
		addr = parseAddr(self.addr)
		if addr[0]=='unix':
			self.reader, self.writer = await asyncio.open_unix_connection(addr[1])
		elif addr[0]=='tcp':
			self.reader, self.writer = await asyncio.open_connection(addr[1], addr[2])
		self.writer.write(self.PROTOCOL_TYPE.to_bytes(4, byteorder='little'))
		self.input_handler=self.loop.create_task(self.handleInput())
	
	def connectBlocking(self):
		self.loop.run_until_complete(self.connect())
	
	def _getEndpointMaps(self):
		return (self.link_endpoints, )


class SrvMsgClient(BaseMsgClient, _LinkEndpointsMixin, _MultipleEndpointMapHandlerMixin):
	
	def __init__(self, reader, writer, global_endpoints={}, *args, **kwargs):
		super(SrvMsgClient, self).__init__(*args, **kwargs)
		self.reader = reader
		self.writer = writer
		self.answer_token = 0
		self.answer_futures = {}
		self.input_handler = None
		self.global_endpoints = global_endpoints
		
	def startHandleInput(self):
		self.input_handler=self.loop.create_task(self.handleInput())
		return self.input_handler
	
	def _getEndpointMaps(self):
		return (self.link_endpoints, self.global_endpoints)
